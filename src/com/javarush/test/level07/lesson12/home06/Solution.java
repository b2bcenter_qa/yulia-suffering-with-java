package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human). Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

public class Solution
{
    public static void main(String[] args)
    {
        Human humanGPF = new Human("ДедПапин", true, 70);
        Human humanGMF = new Human("БабаПапина", false, 69);
        Human humanGPM = new Human("ДедМамин", true, 60);
        Human humanGMM = new Human("БабаМамина", false, 59);
        Human humanF = new Human("Папа", true, 40, humanGPF, humanGMF);
        Human humanM = new Human("Мама", false, 39, humanGPM, humanGMM);
        Human humanCh1 = new Human("Ребенок1", true, 20, humanF, humanM);
        Human humanCh2 = new Human("Ребенок2", false, 18, humanF, humanM);
        Human humanCh3 = new Human("Ребенок3", true, 10, humanF, humanM);

        System.out.println(humanGPF.toString());
        System.out.println(humanGMF.toString());

        System.out.println(humanGPM.toString());
        System.out.println(humanGMM.toString());

        System.out.println(humanF.toString());
        System.out.println(humanM.toString());

        System.out.println(humanCh1.toString());
        System.out.println(humanCh2.toString());
        System.out.println(humanCh3.toString());



    }

    public static class Human
    {
        String name;
        boolean sex;
        int age;
        Human father;
        Human mother;

        public Human(String name, boolean sex, int age)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }
        public Human(String name, boolean sex, int age, Human father, Human mother)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }

}
