package com.javarush.test.level07.lesson04.task05;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* Один большой массив и два маленьких
1. Создать массив на 20 чисел.
2. Ввести в него значения с клавиатуры.
3. Создать два массива на 10 чисел каждый.
4. Скопировать большой массив в два маленьких: половину чисел в первый маленький, вторую половину во второй маленький.
5. Вывести второй маленький массив на экран, каждое значение выводить с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        int [] arrayBig = new int[20];
        int [] arrayS1 = new int[10];
        int [] arrayS2 = new int[10];

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < arrayBig.length; i++)
        {
            arrayBig[i]= Integer.parseInt(reader.readLine());
        }
        for (int i = 0; i < arrayBig.length/2; i++)
        {
            arrayS1[i] = arrayBig[i];
            arrayS2[i] = arrayBig[i+10];
        }

        /*for (int i = 0; i < arrayS2.length; i++)
        {
            arrayS2[i] = arrayBig[i+10];
        }*/

        for (int i = 0; i < arrayS2.length; i++)
        {
            System.out.println(arrayS2[i]);
        }

    }
}
